import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'placeHolder'
})
export class placeHolderPipe implements PipeTransform {
  transform(value: string, defa:string): string {
    return (value)? value: defa ;
  }
}
