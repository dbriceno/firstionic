import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

//Services
import { ListaDeseosService } from '../app/services/lista-deseos.service';

//Pipe
import { placeHolderPipe } from '../app/pipes/placeHolder.pipe';
import { PendientesPipe } from '../app/pipes/pendientes.pipe';
//Pages
import { TabsPage } from '../pages/tabs/tabs';
import { AddListaComponent } from '../pages/addLista/addLista.component';
import { PendientesComponent } from '../pages/pendientes/pendientes.component';
import { TerminadosComponent } from '../pages/terminados/terminados.component';
import { DetailleComponent } from '../pages/detaille/detaille.component';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

@NgModule({
  declarations: [
    MyApp,
    placeHolderPipe,
    PendientesPipe,
    PendientesComponent,
    TerminadosComponent,
    AddListaComponent,
    DetailleComponent,
    TabsPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    PendientesComponent,
    TerminadosComponent,
    AddListaComponent,
    DetailleComponent,
    TabsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    ListaDeseosService,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
