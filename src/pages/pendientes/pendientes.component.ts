import { Component, OnInit } from '@angular/core';
import { ListaDeseosService } from '../../app/services/lista-deseos.service';



import { NavController } from 'ionic-angular';
import { AddListaComponent } from '../../pages/addLista/addLista.component';
import { DetailleComponent } from '../../pages/detaille/detaille.component';

@Component({
  selector: 'app-pendientes',
  templateUrl: 'pendientes.component.html',
})
export class PendientesComponent implements OnInit {
  constructor(private _listaDeseos: ListaDeseosService,
              private NavCtrl: NavController) {
  }

  ngOnInit(){}


  goToAddPage(){
    console.log(this._listaDeseos.listas)
    this.NavCtrl.push(AddListaComponent);
  }


  showDetail(lista, idx){
    console.log("Go to DetailsPage");
    this.NavCtrl.push(DetailleComponent,{lista,idx});

  }

}
