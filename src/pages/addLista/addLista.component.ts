import { Component, OnInit } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { Lista, ListaItem } from '../../app/clases/index';

import { ListaDeseosService } from '../../app/services/lista-deseos.service';




@Component({
  selector: 'app-addLista',
  templateUrl: 'addLista.component.html',
})
export class AddListaComponent implements OnInit {

  nombreLista:string;
  nombreItem:string = "";

  items:ListaItem[] = [];

  constructor(
    public alertCtrl:AlertController,
    public navCtrl:NavController,
    public _listaDeseos:ListaDeseosService
  ) {  }

    ngOnInit(){}

    addItem(){
      if (this.nombreItem.length == 0) {
        console.log("Rien à ajouter");
        return;
      }
      let item = new ListaItem();
      item.nombre = this.nombreItem;
      this.items.push(item);
      this.nombreItem = "";
    }

    addList(){
      if (this.nombreLista.length == 0) {
        let alert = this.alertCtrl.create({
          title: 'Name of the list',
          subTitle: 'The name of the list is an obligation for add a new list',
          buttons: ['Ok']
        });
        alert.present();
        return;
      }
      //Creation d'une variable de type Liste et on ajoute les variables
      let lista = new Lista(this.nombreLista);
      lista.items = this.items;

      //On envoi dans la liste du service variable _listaDeseos la variable de let au dessus
      //this._listaDeseos.listas.push(lista);
      this._listaDeseos.addLista(lista); //Avec LocalStorage
      this.navCtrl.pop();  //On quitte la page et on retourne à l'autre

    }

    suprItem(idx:number){
      this.items.splice(idx, 1);
    }


}
