import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { Lista, ListaItem } from '../../app/clases/index';
import { ListaDeseosService } from '../../app/services/lista-deseos.service';

@Component({
  selector: 'app-detaille',
  templateUrl: 'detaille.component.html',
})
export class DetailleComponent implements OnInit {

  idx:number;
  lista:Lista;

  constructor(public navCtrl:NavController,
              public navParams:NavParams,
              public alertCtrl:AlertController,
              public _listaDeseos:ListaDeseosService){

  this.idx = this.navParams.get("idx");
  this.lista = this.navParams.get("lista");
  }

  ngOnInit() {}

  actualiserItem(item:ListaItem){
    item.completado = !item.completado;

    let todosMarcados = true;
    for(let item of this.lista.items){
      if (!item.completado) {
          todosMarcados = false;
          break;
      }
    }

    this.lista.terminada = todosMarcados;

    this._listaDeseos.actualizarData();

  }

  deleteListe(){
    let confirm = this.alertCtrl.create({
    title: 'Delete this list ?',
    message: 'Do you agree to delete this list and those items?',
    buttons: [
      {
        text: 'Disagree',
        handler: () => {
          return;
        }
      },
      {
        text: 'Agree',
        handler: () => {
          this._listaDeseos.suprLista(this.idx);
          this.navCtrl.pop();
        }
      }
    ]
  });
  confirm.present();
}


}
