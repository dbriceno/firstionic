import { Component, OnInit } from '@angular/core';
import { ListaDeseosService } from '../../app/services/lista-deseos.service';



import { NavController } from 'ionic-angular';
import { DetailleComponent } from '../../pages/detaille/detaille.component';

@Component({
  selector: 'app-terminados',
  templateUrl: 'terminados.component.html',
})
export class TerminadosComponent implements OnInit {
  constructor(private _listaDeseos:ListaDeseosService,
              private NavCtrl:NavController) {  }

  ngOnInit() {}

  showDetail(lista, idx){
    console.log("Go to DetailsPage");
    this.NavCtrl.push(DetailleComponent,{lista,idx});

  }
}
